<?php

namespace formation\AnnonceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use formation\AnnonceBundle\Form\AnnonceType;
use formation\AnnonceBundle\Entity\Annonce;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('formationAnnonceBundle:Default:index.html.twig');
    }
    public function testAction($prenom)
    {
        return $this->render('formationAnnonceBundle:Default:test.html.twig',array('p'=>$prenom));
    }
    public function sommeAction($a,$b)
    {
       
        $s=$a+$b;
        
        return $this->render('formationAnnonceBundle:Default:somme.html.twig',array('s'=>$s));
    }
    public function ajouterAction()
    {
       
        $annonce= new Annonce();
        $message ='';
        $doctrine=$this->getDoctrine();
        $em=$doctrine->getManager();
        $form=$this->createForm(new AnnonceType(),$annonce);
        $request=$this->getRequest();
        if ($request->getMethod() =='POST')
        {
            $form->bind($request);
            $em->persist($annonce);
            $em->flush();
            $request->getSession()
                    ->getFlashBag()
                    ->add('success','une nouvelle annonce a été ajoutée avec succés!');
           
            $annonce=new Annonce();
            $form=$this->createForm(new AnnonceType(),$annonce);
            
        }
        
        return $this->render('formationAnnonceBundle:Annonce:ajout.html.twig',array('form'=>$form->createView()));
    }
    public function listerAction()
    {
        $em=$this->getDoctrine()->getManager();
        $rep=$em->getRepository ("formationAnnonceBundle:Annonce");
        $annonces= $rep->findAll();
        return $this->render('formationAnnonceBundle:Annonce:lister.html.twig', array('annonces'=>$annonces));
    }
    public function modifierAction($id)
    {
        
        $em=$this->getDoctrine()->getManager();
        $annonce=$em->getRepository ("formationAnnonceBundle:Annonce")->find($id);
        $form=$this->createForm(new AnnonceType(),$annonce);
        $request=$this->getRequest();
        if ($request->getMethod()=='POST')
        {
            $form->bind($request);
            if($form ->isValid())
            {
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success','une nouvelle annonce a été ajoutée avec succés!');
            }
        }
       return $this->render('formationAnnonceBundle:Annonce:ajout.html.twig',array('form'=>$form->createView()));
    }
    public function supprimerAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $rep=$em->getRepository ("formationAnnonceBundle:Annonce");
        
        $annonce= $rep->find($id);
        $em->remove($annonce);
        $em->flush();
        $url=   $this->generateUrl('formation_annonce_lister');
        return $this->redirect($url);
        //return $this->render('FormationAnnonceBundle:Annonce:lister.html.twig', array('annonces'=>$annonces));
    }
    public function TopAction()
    {
        $em=$this->getDoctrine()->getManager();
        $query=$em->createQuery(
                'select A from formationAnnonceBundle:Annonce A 
                ORDER BY A.id DESC ')->setMaxResults(3);
        $Annonces=$query->getResult();
        return $this->render('formationAnnonceBundle:Annonce:lister.html.twig', array('annonces'=>$Annonces));
        }
    public function TextAction()
    {
        //on récupére le service
        $verif=$this->container->get('formation_annonce_verif');
        $text= 'bonjour';
        if($verif->isInferieur($text))
        {
            throw new \Exception('votre message est inférieur à 50 caractére!');
        }
        }
        
}
