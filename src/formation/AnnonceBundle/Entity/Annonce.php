<?php

namespace formation\AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Annonce
 *
 * @ORM\Table(name="annonce")
 * @ORM\Entity(repositoryClass="formation\AnnonceBundle\Repository\AnnonceRepository")
 */
class Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="text")
     * @Assert\NotBlank()
     * @Assert\Length(
     * min="3",
     * max="20",
     * minMessage="le nom doit avoir au moins {{limit}}caractére ",
     * maxMessage="le nom doit avoir au maximum {{limit}} caractére")
     */
    private $titre;
    

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=255)
     */
    private $contenu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", length=255)
     */
    private $auteur;
    /**
     * @ORM\ManyToOne(targetEntity="formation\AnnonceBundle\Entity\Categorie")
     * 
     */
    private $Categorie;
    /**
     * 
     *
     * @ORM\ManyToMany(targetEntity="formation\AnnonceBundle\Entity\Candidature")
     */
    private $Condidature;

    /**
     * Get id
     *
     * @return integer 
     */
    
   
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Annonce
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Annonce
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Annonce
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     * @return Annonce
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set Categorie
     *
     * @param \formation\AnnonceBundle\Entity\Categorie $categorie
     * @return Annonce
     */
    public function setCategorie(\formation\AnnonceBundle\Entity\Categorie $categorie = null)
    {
        $this->Categorie = $categorie;

        return $this;
    }

    /**
     * Get Categorie
     *
     * @return \formation\AnnonceBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->Categorie;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Condidature = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add Condidature
     *
     * @param \formation\AnnonceBundle\Entity\Candidature $condidature
     * @return Annonce
     */
    public function addCondidature(\formation\AnnonceBundle\Entity\Candidature $condidature)
    {
        $this->Condidature[] = $condidature;

        return $this;
    }

    /**
     * Remove Condidature
     *
     * @param \formation\AnnonceBundle\Entity\Candidature $condidature
     */
    public function removeCondidature(\formation\AnnonceBundle\Entity\Candidature $condidature)
    {
        $this->Condidature->removeElement($condidature);
    }

    /**
     * Get Condidature
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCondidature()
    {
        return $this->Condidature;
    }
}
