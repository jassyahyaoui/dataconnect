<?php
// src/AppBundle/Entity/User.php

namespace formation\utlisateurBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="User")
 * @ORM\Entity(repositoryClass="formation\utlisateurBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="DataConnectBundle\Entity\Company", cascade={"persist"})
     */    
    protected $company;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
         $this->company = new ArrayCollection();
    }
    

    /**
     * Add company
     *
     * @param \DataConnectBundle\Entity\Company $company
     * @return User
     */
    public function addCompany(\DataConnectBundle\Entity\Company $company)
    {
        $this->company[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \DataConnectBundle\Entity\Company $company
     */
    public function removeCompany(\DataConnectBundle\Entity\Company $company)
    {
        $this->company->removeElement($company);
    }

    /**
     * Get company
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
