<?php

namespace formation\utlisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
       
        return $this->render('utilisateurBundle:Default:index.html.twig');
    }
}
