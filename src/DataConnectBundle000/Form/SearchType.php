<?php

namespace DataConnectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class SearchType extends AbstractType
{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                

               ->add('user', TextType::class,array( 'required' => false))
               ->add('month', TextType::class)
               ->add('save', SubmitType::class, array('label' => 'Envoyer'))
                ->add('api', ChoiceType::class, array(
                         'choices' => array(
                                'Preprod' => 'Staging Api ',
                                 'Prod' => 'Production Api '),
                          'placeholder' => 'Choisissez votre API'))
                 ->add('userApi', TextType::class,array( 'required' => false))
                  ->add('cle', TextType::class,array( 'required' => false));
//         ->add('ftp', TextType::class,array( 'required' => true))
//                 ->add('ftpuser', TextType::class,array( 'required' => true))
//                 ->add('ftppw', TextType::class,array( 'required' => true));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dataconnectbundle_search';
    }

    

}
