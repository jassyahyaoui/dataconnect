<?php

namespace DataConnectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                

               ->add('login', TextType::class,array( 'required' => true))
               ->add('email', EmailType::class,array( 'required' => true))
               ->add('password', PasswordType::class,array( 'required' => true))
                ->add('role', ChoiceType::class, array(
                         'choices' => array(
                                'ROLE_USER' => 'User',
                                 'ROLE_ADMIN' => 'Admin'),
                          'placeholder' => 'Choisissez le role'))
               ->add('save', SubmitType::class, array('label' => 'Envoyer'))
;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dataconnectbundle_user_type';
    }

    

}
