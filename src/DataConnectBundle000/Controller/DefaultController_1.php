<?php

namespace DataConnectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DataConnectBundle\Form\SearchType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use phpseclib\Net\SFTP;

class DefaultController extends Controller {

    private $curl = null;
    private $user = "api_154374_f04c@api.oneup.com:fe6947444e1411ba2a71e9ee0d73a0b14c5b351b";

    const URL = 'https://api.oneup.com/v1/';
    const IDC = '26cf1649045750c76b9328dedad76a821662f9e6';

    public function __construct($cache_path = './') {


        // Création et configuration de la resource CURL
        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_POST => true, // Méthode POST
            CURLOPT_RETURNTRANSFER => true, // On récupère directement les données
            CURLOPT_FAILONERROR => true, // Pas de données s'il y a un code d'erreur HTTP
            CURLOPT_ENCODING => 'gzip', // Compression gzip
            CURLOPT_USERAGENT => 'Mozilla/5.0 (' . trim(`uname -ms`) . '; PHP ' . PHP_VERSION . ') ', // Agent
            CURLOPT_HTTPHEADER => array('X-IDC: ' . self::IDC, 'Accept: application/json'), // Format des données
            CURLOPT_SSL_VERIFYPEER => false,
        ));
    }

    public function indexAction(Request $request) {
      

//$stream = fopen("ssh2.sftp://$sftp/lamp0/web/vhosts/dev.lesaides.pmeti.hosting/htdocs/export.json", 'w+');

        
        
        
        $url = "https://api.oneup.com/v1/transactions";
        $username = "api_154374_f04c@api.oneup.com";
        $password = 'fe6947444e1411ba2a71e9ee0d73a0b14c5b351b';
        $transactions = $this->getTransaction();
       //dump($transactions);die();  //afficher le tableau contenant la liste des transactions
       

        $searchForm = $this->createFormBuilder()
                ->add('user', TextType::class)
                ->add('cle', TextType::class)
                ->add('mois', ChoiceType::class, array(
                    'choices' => array(
                        1 => 'janvier',
                        2 => 'fevrier',
                        3 => 'Mars',
                         4 => 'Avril',
                        5 => 'Mai',
                        6 => 'Juin',
                         7 => 'Juiller',
                        8 => 'Aout',
                        9 => 'Septembre',
                         10 => 'Octobre',
                        11 => 'Novembre',
                        12 => 'Decembre',
                    ),
                ))
                ->add('save', SubmitType::class, array('label' => '€nvoyer'))
                ->getForm();

        $searchForm->handleRequest($request); //recup donnes
        if ($searchForm->isSubmitted()) {
//            $username = $searchForm['user']->getData();
//            $password = $searchForm['cle']->getData();
            $mois = $searchForm['mois']->getData();
            // dump(date("n", strtotime('2016-10-21')));
            // dump($mois);die;
                $journeaux = array(); //mois
                $attachments = array();
                //$mois = '2016-10-21';
                //dump($mois);die();
                foreach ($transactions as $key => $value) {
                    
                   // dump($this->getAttachments($value['piece_number'])) ;die();
                    //dump(date("n", strtotime($value['date'])));die();
                    if ($mois == date("n", strtotime($value['date']))) {
                        $journeaux[] = $value;
                        //  dump(json_encode($value,true));die();
                        $q=$value['piece_number']; //récupérer le piece_number pour l'endpoint de téléchargement des piéces jointes.
                        $attachments = $this->getAttachments($q);
                        //dump($attachments);die(); //pour afficher les piéces  attachée

                        // dump($transactions[$key]);die();
                    }
                }
//            } else {
//                // throw new \Exception(curl_error($this->curl), curl_errno($this->curl));
//            }
            //   dump(json_encode($journeaux,true));die();
            //  $response = $this->exportCSV($journeaux); pour exporter un fichier csv
            $response = $this->exportJSON(json_encode($journeaux, true)); //convertir un rendu array en json
           //dump($response)  ;die;
            return $this->render('DataConnectBundle:Default:index.html.twig', array(
                    'response' => $response,
                    'attachments' => $attachments,
                    'searchForm' => $searchForm->createView()

        ));



//return $response;
        }//end submit
        return $this->render('DataConnectBundle:Default:index.html.twig', array(
                    'searchForm' => $searchForm->createView(),
        ));
    }

    public function exportCSV($journeaux) {
        $response = new StreamedResponse();
        $response->setCallback(function() use($journeaux) {
            $handle = fopen('php://output', 'w+');
            // Add the header of the CSV file
            fputcsv($handle, array('id', 'date'), ';');
            foreach ($journeaux as $key => $row) {
                fputcsv(
                        $handle, // The file pointer
                        array($row['id'], $row['date']), // The fields
                        ';' // The delimiter
                );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }

    public function exportJSON($journeaux) {
        $response = new StreamedResponse();
        $filepath ='';
         $sftp = new SFTP('sftp.dc0.gpaas.net');
        
        if (!$sftp->login('6381035', '.]gX4h{j3S4dV6J')) {
            throw new \Exception('Cannot login into your server !');
        }
        $sftp->chdir('lamp0/web/vhosts/dev.lesaides.pmeti.hosting/htdocs/symfony28/web/bundles/export/');
        //$response->setCallback(function() use($journeaux, $sftp, $filepath) {
            //$handle = fopen('php://output', 'w+');//dossier telechargement
            //$handle = fopen('C:\\test\export.json', 'w+');//dans un dossier local

            //ecriture en ligne/serveur
            $nom_file = 'export_'.time().'.json';
            $filepath = 'lamp0/web/vhosts/dev.lesaides.pmeti.hosting/htdocs/symfony28/web/bundles/export/'.$nom_file;
            //dump('ddd');die;
            $sftp->put($nom_file, $journeaux);  

            //fwrite($handle, $journeaux);
            //fclose($handle);
        //});
        //$response->setStatusCode(200);
        //$response->headers->set('Content-Type', 'application/force-download');
        //$response->headers->set('Content-Disposition', 'attachment; filename="export.json"');

        return $nom_file;
    }

    public function getTransaction() {
        $url = "https://api.oneup.com/v1/transactions";
        $username = "api_154374_f04c@api.oneup.com";
        $password = 'fe6947444e1411ba2a71e9ee0d73a0b14c5b351b';
        /*   code Arthur   */
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);   //cette ligne affiche la liste de tous les journaux
        //dump($result);die();
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($result));
        //   dump(curl_setopt($curl, CURLOPT_POSTFIELDS, json_decode($result)));die();
        // ferme la ressource cURL et libère les ressources systèmes
        return json_decode($result, true);
    }

    public function getAttachments($q) {
        $url = "https://api.oneup.com/v1/attachments";
        $username = "api_154374_f04c@api.oneup.com";
        $password = 'fe6947444e1411ba2a71e9ee0d73a0b14c5b351b';
        /*   code Arthur   */
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $q);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);   //cette ligne affiche la liste de tous les journaux
        //dump($result);die();
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($result));
        //   dump(curl_setopt($curl, CURLOPT_POSTFIELDS, json_decode($result)));die();
        // ferme la ressource cURL et libère les ressources systèmes
        return json_decode($result, true);
    }

}
