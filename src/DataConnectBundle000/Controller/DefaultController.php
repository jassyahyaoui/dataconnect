<?php

namespace DataConnectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DataConnectBundle\Form\SearchType;
use DataConnectBundle\Form\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use phpseclib\Net\SFTP;
use DataConnectBundle\Entity\LogFile;

//use DataConnectBundle\Service\LogService;
//use JMS\SecurityExtraBundle\Annotation\Secure;

class DefaultController extends Controller {

    private $curl = null;
    private $user = "api_154374_f04c@api.oneup.com:fe6947444e1411ba2a71e9ee0d73a0b14c5b351b";

    const URL = 'https://erp-api.fabereo.com/';
    const IDC = '26cf1649045750c76b9328dedad76a821662f9e6';

    public function __construct($cache_path = './') {


        // Création et configuration de la resource CURL
        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_POST => true, // Méthode POST
            CURLOPT_RETURNTRANSFER => true, // On récupère directement les données
            CURLOPT_FAILONERROR => false, // Pas de données s'il y a un code d'erreur HTTP
            CURLOPT_ENCODING => 'gzip', // Compression gzip
            CURLOPT_USERAGENT => 'Mozilla/5.0 (' . trim(`uname -ms`) . '; PHP ' . PHP_VERSION . ') ', // Agent
            CURLOPT_HTTPHEADER => array('X-IDC: ' . self::IDC, 'Accept: application/json'), // Format des données
            CURLOPT_SSL_VERIFYPEER => false,
        ));
    }

//    /**
//     * @Secure(roles="ROLE_USER")
//     */
    public function indexAction(Request $request) {

        $logger = $this->get('dataconnectbundle_log_file');
//        //$logger->info('I just got the logger');
//        $loLogger = $this->get('logger');
//        $loLogger->info('Composant Logger chargé');
//        $loLogger->error('Houston, nous avons un problème');

        $searchForm = $this->createForm(new SearchType());
        $searchForm->handleRequest($request); //recup donnes
        $mm = ''; //récupération du numéro de mois à utiliser dans l'arborescence de json
        $yy = '';
        $jj = '';
        $hostFtp = 'sftp.dc0.gpaas.net';
        $userFtp = '6381035';
        $passFtp = '.]gX4h{j3S4dV6J';
        $parentFolder = 'lamp0/web/vhosts/dataconnect.pmeti.hosting/htdocs/symfony28/web/bundles/export/';
        $iso = 'FR';
        if ($searchForm->isSubmitted()) {
            $user = $searchForm['user']->getData();
            $api = $searchForm['api']->getData();
            $userApi = $searchForm['userApi']->getData();
            $clef = $searchForm['cle']->getData();
            $question = $request->request->get('question');

            if ($api == 'Preprod') {
                if ($question == 'non') {
                    $username = 'api_fabereo_' . $user . '@erp-api.fabereo.com';
                    $s = hash_hmac('sha256', $username, 'wJwJP$F@U55c5Zx-', true);
                    $password = base64_encode($s);
                } else {
                    $username = $userApi;
                    $password = $clef;
                    $user = strstr($username, '@', true);
                }
                $url = 'https://stagingapi.oneup.com/v1/';
                $urlAttach = "https://staging.oneup.com/attachments/";
            } else if ($api == 'Prod') {
                if ($question == 'non') {
                    $username = 'api_fabereo_' . $user . '@erp-api.fabereo.com';
                    $s = hash_hmac('sha256', $username, '1siH23sas872b1!sad-*$dwqsh830h', true);
                    $password = base64_encode($s);
                } else {
                    $username = $userApi;
                    $password = $clef;
                    $user = strstr($username, '@', true);
                }
                $url = 'https://erp-api.fabereo.com/v1/';
                $urlAttach = "https://erp.fabereo.com/attachments/";
            }

            $transactions = $this->getTransaction($url, $username, $password); //afficher le tableau contenant la liste des transactions 
            //dump($transactions);die();
            $logger->saveLog($transactions['code'], $url, $username, $password);


            $month = $searchForm['month']->getData();

            $mm = substr($month, 5, 2); //récupération du numéro de mois à utiliser dans l'arborescence de json
            $yy = substr($month, 0, 4); //récupération de l'année à utiliser dans l'arborescence de json
            $journeaux = array(); //mois
            $q = '';
            $attachments = array();
            if ($transactions['result'] == '') { //si le code company est invalide cad si le resultat retourné des transactions est vide
                return $this->render('DataConnectBundle:Default:index.html.twig', array(
                            'response' => '',
                            'attachments' => '',
                            'piece_number' => '',
                            'message' => 'code company non valide',
                            'searchForm' => $searchForm->createView(),
                            'mois' => '',
                            'year' => ''
                ));
            } else {
                foreach ($transactions['result'] as $key => $value) {
                    // dump($value);
                    $m = substr($value['date'], 5, 2);
                    $y = substr($value['date'], 0, 4);
                    $monthApi = $y . '-' . $m;
                    //  dump( date("n-Y", strtotime($value['date'])));
                    // dump(date("n-Y", strtotime($value['date'])));die();
                    if ($month == $monthApi) {

                        $journeaux[] = $value;
//dump($journeaux);
                        //  dump(json_encode($value,true));//die();
                        $q = $value['piece_number']; //récupérer le piece_number pour l'endpoint de téléchargement des piéces jointes.
                        $attachments = $this->getAttachments($url, $q, $username, $password);
                        //  dump($attachments);die();
                        // dump($transactions[$key]);die();
                    } else {
                        $m = $mm;
                        $y = $yy;
                    }
                }
                $sftp = $this->connectToServer($hostFtp, $userFtp, $passFtp);
                $sftp = $this->createFolder($sftp, $parentFolder, $iso, $y, $m, $user);
                //die();
                //  dump($yy,$mm); die();
                // dump($journeaux);die;
                $nom_file = $iso . '_' . $y . '_' . $m . '_' . $user . '_' . time() . '.json';
                $filepath = 'symfony28/web/bundles/export/' . $iso . '/' . $y . '/' . $m . '/' . $user . '/' . $nom_file;
                $responseCSV = $this->exportCSV($sftp, $parentFolder, $y, $m, $user, $journeaux, $attachments, $urlAttach, $q); //pour exporter un fichier csv
                //return $responseCSV;
                // dump($responseCSV);die();
                // $response = $this->exportJSON($sftp, $parentFolder, $attachments, $urlAttach, $q, $y, $m, $user, json_encode($journeaux, true)); //convertir un rendu array en json
                //$reponse = 
                //dump(json_encode($journeaux, true))  ;die();
                // return $responseCSV;

//                
                $params =array(
                            'response' => $responseCSV,
                            //  'response' => $response,
                            'attachments' => $attachments,
                            'piece_number' => $q,
                            'message' => '',
                            'searchForm' => $searchForm->createView(),
                            'mois' => $m,
                            'year' => $y
                );
                
                $response = new Response();
        $response->setContent('DataConnectBundle:Default:index.html.twig', $params);
        $response->headers->set('Content-Type', 'application/pdf'); 
            return   $response;
            }

//return $response;
        }//end submit
        return $this->render('DataConnectBundle:Default:index.html.twig', array(
                    'searchForm' => $searchForm->createView(),
                    'mois' => $mm,
                    'year' => $yy,
                    'jour' => $jj
        ));
    }

    public function exportCSV($sftp, $parentFolder, $y, $m, $user, $journeaux, $attachments, $urlAttach, $q) {

        $response = new StreamedResponse();
        $filepath = '';
        $iso = 'FR';

        //$sftp = $this->createFolder($sftp, $parentFolder ,$iso, $y, $m, $user);
        $nom_file = $iso . '_' . $y . '_' . $m . '_' . $user . '_' . time() . '.csv';
        $filepath = 'symfony28/web/bundles/export/' . $iso . '/' . $y . '/' . $m . '/' . $user . '/' . $nom_file;
        // $response->setCallback(function() use($journeaux, $sftp, $nom_file) {
        $handle = fopen($nom_file, 'w');
//           $handle = fopen('ssh2.sftp://'.$sftp.'/'.$dest, 'w');
        //$handle = fopen('php://output', 'w+');
        // Add the header of the CSV file
        fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF)); //pour l'encodage utf8
        fputcsv($handle, array('code', 'Id', 'Piece_number', 'Status', 'Date', 'label', 'Account label',
            'way', 'amount', 'currency_iso_code'), ';');

        foreach ($journeaux as $key => $row) {
            foreach ($row['entries'] as $key => $entry) {
                fputcsv(
                        $handle, // The file pointer
                        array($row['journal']['code'],
                    $row['id'],
                    $row['piece_number'],
                    $row['status'],
                    $row['date'],
                    $entry['label'],
                    $entry['account_label'],
                    $entry['way'],
                    $entry['amount'],
                    $row['currency_iso_code'],
                        ), // The fields
                        ';' // The delimiter
                );
            }
        }


        fclose($handle);
        $filecontent = file_get_contents($nom_file);
        // dump($filecontent);die();
        $sftp->put($nom_file, $filecontent);
        //  });
        //  $response->setStatusCode();
        // $response->headers->set('Content-Type', 'application/force-download');
        $filename = $iso . '_' . $y . '_' . $m . '_' . $user . '_' . time() . '.csv';
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $filename);
        foreach ($attachments as $key => $row) {
            $m_att = substr($row['updated_at'], 5, 2);
            $y_att = substr($row['updated_at'], 0, 4);
            $monthAttach = $y_att . '-' . $m_att;
            $test = $y . '-' . $m;
            // dump('les mois de la piéce jointe est'.$monthAttach,'le mois de test est'.$test);
            if ($monthAttach == $test) {

                //   $imageString = file_get_contents("https://erp.fabereo.com/attachments/" . $row['public_key']);
                $imageString = file_get_contents($urlAttach . $row['public_key']);
                $sftp->put($iso . '_' . $y . '_' . $m . '_' . $user . '_attachment_' . $q . '_' . $row['name'], $imageString);
            }
        }
        return array($filename, $filepath);
        // return $response;
    }

    public function connectToServer($hostFtp, $userFtp, $passFtp) {

        $sftp = new SFTP($hostFtp);
        if (!$sftp->login($userFtp, $passFtp)) {
            throw new \Exception('Cannot login into your server !');
        }

        return $sftp;
    }

    public function createFolder($sftp, $parentFolder, $iso, $y, $m, $user) {
        $sftp->chdir($parentFolder); //création du dossier distant qui contiendra tous les journaux
        $sftp->mkdir($iso);
        $sftp->mkdir($iso . '/' . $y);
        $sftp->mkdir($iso . '/' . $y . '/' . $m);
        $sftp->mkdir($iso . '/' . $y . '/' . $m . '/' . $user);
        $sftp->chdir($iso . '/' . $y . '/' . $m . '/' . $user . '/');

        return $sftp;
    }

    public function exportJSON($sftp, $parentFolder, $attachments, $urlAttach, $q, $y, $m, $user, $journeaux) {
        ini_set("allow_url_fopen", 1);
        $response = new StreamedResponse();
        $filepath = '';
        $iso = 'FR';

        //$sftp = $this->createFolder($sftp, $parentFolder ,$iso, $y, $m, $user);
        //ecriture dans le serveur sftp
        $nom_file = $iso . '_' . $y . '_' . $m . '_' . $user . '_' . time() . '.json';
        $filepath = 'symfony28/web/bundles/export/' . $iso . '/' . $y . '/' . $m . '/' . $user . '/' . $nom_file;
        $sftp->put($nom_file, $journeaux);

        foreach ($attachments as $key => $row) {

            //   $imageString = file_get_contents("https://erp.fabereo.com/attachments/" . $row['public_key']);
            $imageString = file_get_contents($urlAttach . $row['public_key']);
            $sftp->put($iso . '_' . $y . '_' . $m . '_' . $user . '_attachment_' . $q . '_' . $row['name'], $imageString);
        }
        return array($nom_file, $filepath);
        //return($nom_file);
    }

    public function getTransaction($url, $username, $password) {
        //  $url = "https://api.oneup.com/v1/transactions";
        // $url = "https://erp-api.fabereo.com/v1/transactions"; //production api host
        $trans = "transactions";
        /*   code CURL   */
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url . $trans);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //code copié
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        ///
        $result = curl_exec($curl);   //cette ligne affiche la liste de tous les journaux
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($curl);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($result));
        //dump($http_status);die();
        if ($http_status == 401) {
            echo "HTTP Status == 401 <br/>";
            echo "Curl Errno returned $curl_errno <br/>";
        } else {
            // ferme la ressource cURL et libère les ressources systèmes
            //    dump(json_decode($result, true));die();
            //return json_decode($result, true);
            return array('result' => json_decode($result, true), 'code' => $http_status);
        }
    }

    public function getAttachments($url, $q, $username, $password) {
        //  $url = "https://api.oneup.com/v1/attachments";
        // $url = "https://erp-api.fabereo.com/v1/attachments"; // production api host
        $attach = "attachments";
        /*   code CURL   */
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url . $attach);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $q);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);

        $result = curl_exec($curl);   //cette ligne affiche la liste de tous les attachements
        //dump($result);die();
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($curl);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($result));
        if ($http_status == 401) {

            echo "HTTP Status == 401 <br/>";
            echo "Curl Errno returned $curl_errno <br/>";
        } else {
            // ferme la ressource cURL et libère les ressources systèmes
            return json_decode($result, true);
        }
    }

//    public function logAttachmentAction(Request $request) {
//        $date = $request->get('date');
//        $fichier = $request->get('fichier');
//        //dump($date);die();
//        $logger = $this->get('dataconnectbundle_log_file');
//        //$logger->info('I just got the logger');
//        $logger->saveLogAttachment($date, $fichier);
//        $return = array('success' => 1);
//        $response = new Response(json_encode($return));
//        $response->headers->set('Content-Type', 'application/json');
//        return $response;
//    }

    public function listerAction() {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $rep = $em->getRepository("DataConnectBundle:LogFile");

        $logs = $rep->findAll();
        //  dump($logs); die();
        return $this->render('DataConnectBundle:Default:lister.html.twig', array('logs' => $logs));
    }

    public function addUserAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $userForm = $this->createForm(new UserType());
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted()) {
            $login = $userForm['login']->getData();
            $email = $userForm['email']->getData();
            $password = $userForm['password']->getData();
            $role = $userForm['role']->getData();

            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setUsername($login);
            $user->setEmail($email);
            $user->setRoles([$role]);
                //$factory = $this->get('security.encoder_factory');
                //$encoder = $factory->getEncoder($user);
                //$password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPlainPassword($password);
            $user->setEnabled(true);
            $userManager->updateUser($user);
            $this->getDoctrine()->getManager()->flush();
            // set flash messages
            //$request->getSession()->getFlashBag()->add('notice', 'Profile created');
             $this->addFlash(
            'notice',
            'Your changes were saved!'
        );
             return $this->redirectToRoute('data_connect_add_user');
        }
        return $this->render('DataConnectBundle:Default:newUser.html.twig', array(
                    'userForm' => $userForm->createView(),
        ));
    }
    public function listerUserAction() {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();

        //$users = $rep->findAll();
        //  dump($logs); die();
        return $this->render('DataConnectBundle:Default:listerUser.html.twig', array('users' => $users));
    }
   

}
