<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Form\SearchType;
use ApiBundle\Services\LesAides;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    public function pageResultatsAction(Request $request) { //Affichage de la liste des dispositifs
        return $this->render('ApiBundle:Default:pageResultats.html.twig', array(
        ));
    }

    public function affichageAideAction($idr = null, $dispositif) {  //Affichage de la fiche d'aide
        $params = array('requete' => $idr, 'dispositif' => $dispositif);
        return $this->render('ApiBundle:Default:affichageAide.html.twig', array('liste' => $this->get('apibundle_lesaides')->AffichageAide($params, true)));
    }

    public function indexAction(Request $request) {
        $lesaidesService = $this->get('apibundle_lesaides')->NAF();
        $searchForm = $this->createForm(new SearchType(new LesAides()), null, array(
            'action' => $this->generateUrl('api_homepage'),
                ))
        ;
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted()) {
            $siret = $searchForm['siret']->getData();
            $domaine = $searchForm['domaine']->getData();
            $departement = $searchForm['departement']->getData();
            $filiere = $searchForm['fliliere']->getData();
            // $ape = $searchForm['activity']->getData();
            $moyen = $searchForm['moyen']->getData();
            $commune = $searchForm['commune']->getData();
            $ape = $request->request->get('codeactiv');
            //si siret pas vide
            if ($siret != '') {
                $params = array('siret' => $siret, 'domaine' => $domaine, 'moyen' => $moyen, 'filiere' => $filiere); //
                
            } elseif($ape != '' && $domaine !='' ) {//on travaille avec activite,domaine,moyen,filiere et departement
                $params = array('ape' => $ape, 'domaine' => $domaine, 'moyen' => $moyen, 'filiere' => $filiere, 'departement' => $departement);
                //  $params = array('ape' => $ape, 'domaine' => $domaine,'moyen' =>$moyen,'filiere' =>$filiere,'departement'=>$departement,'commune'=>$commune,'siret'=>$siret );
        
                }
//dump($params);die();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse($this->get('apibundle_lesaides')->GetListeDispo($params));
            } else {
                return $this->render('ApiBundle:Default:pageResultats.html.twig', array('liste' => $this->get('apibundle_lesaides')->GetListeDispo($params, true))
                );
            }

            //  return new JsonResponse($this->get('apibundle_lesaides')->AffichageAide($no_aide));
        }

        $response =  $this->render('ApiBundle:Default:index.html.twig', array(
                    'searchForm' => $searchForm->createView(),
        ));
        $response->setSharedMaxAge(3600); 

    // (optional) set a custom Cache-Control directive
    $response->headers->addCacheControlDirective('must-revalidate', true);

    return $response;
    }

    public function getNiveauAction(Request $request) {

        $activite = $request->get('activite'); //code
        $index2 = $request->get('index2'); //code
        $index3 = $request->get('index3'); //code
        $index4 = $request->get('index4'); //code
        $index5 = $request->get('index5'); //code
        $index6 = $request->get('index6'); //code
        $niv = $request->get('count'); //code

        $listeActivite = $this->get('apibundle_lesaides')->NAF();


        if ($niv == 2) {
            $res = $listeActivite[$index2]['activites'];
        }
        if ($niv == 3) {
            $res = $listeActivite[$index2]['activites'][$index3]['activites'];
        }
        if ($niv == 4) {
            $res = $listeActivite[$index2]['activites'][$index3]['activites'][$index4]['activites'];
        }
        if ($niv == 5) {
            $res = $listeActivite[$index2]['activites'][$index3]['activites'][$index4]['activites'][$index5]['activites'];
        }
        if ($niv == 6) {
            $res = $listeActivite[$index2]['activites'][$index3]['activites'][$index4]['activites'][$index5]['activites'][$index6]['activites'];
        }
        return new JsonResponse($res);
        //die;
    }

    public function listeDepartement() {

        $tab = array();

        foreach ($this->lesaidesService->Departements() as $ele) {

            $tab[$ele['nom']] = $ele['departement'] . '-' . $tab[$ele['nom']] = $ele['nom'];
        }
    }

}
