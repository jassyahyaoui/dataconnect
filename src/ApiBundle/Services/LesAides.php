<?php
namespace ApiBundle\Services;


class LesAides {
  /**
   * Votre code IDC (à compléter)
   *
   * @var string
   */
  const IDC = '26cf1649045750c76b9328dedad76a821662f9e6'; // Code IDC de démonstration, à remplacer par le votre

  /**
   * URL d'appel de l'API
   * @var string
   */
  const URL = 'https://api.les-aides.fr/';

  /**
   * Répertoire de stockage du cache
   * @var string
   */
  private $cache_path;

  /**
   * Identifiant de resource CURL
   * @var resource
   */
  private $curl = null;

  /**
   * Identifiant de la dernière requête lancée
   * @var integer
   */
  public $requete = 0;

  /**
   * Code APE de l'entreprise
   * @var string
   */
  public $ape = '';

  /**
   * Numéro du domaine d'application de l'aide
   * @var integer
   */
  public $domaine = 0;

  /**
   * Numéro du moyen d'intervention de l'aide
   * @var integer
   */
  public $moyen = 0;

  /**
   * Département d'implantation de l'entreprise
   * @var string
   */
  public $departement = '';

  /**
   * Commune d'implantation de l'entreprise
   * @var integer|string
   */
  public $commune = '';

  /**
   * Filière marché de l'entreprise
   * @var integer
   */
  public $filiere = 0;

  /**
   * SIRET de l'entreprise
   * @var string
   */
  public $siret = '';

  public function getfil() {
      return ['1', '2'];

  }

  /**
   * Constructeur
   * @param string [$cache_path]
   */
  public function __construct($cache_path = './') {
    $this->cache_path = $cache_path;
    if(!is_dir($cache_path)) mkdir($cache_path, 0770, true);

    // Création et configuration de la resource CURL
    $this->curl = curl_init();
    curl_setopt_array($this->curl, array(
        CURLOPT_POST            => true, // Méthode POST
        CURLOPT_RETURNTRANSFER  => true, // On récupère directement les données
        CURLOPT_FAILONERROR     => true, // Pas de données s'il y a un code d'erreur HTTP
        CURLOPT_ENCODING        => 'gzip', // Compression gzip
        CURLOPT_USERAGENT       => 'Mozilla/5.0 ('.trim(`uname -ms`).'; PHP '.PHP_VERSION.') API Les-aides.fr', // Agent
        CURLOPT_HTTPHEADER      => array('X-IDC: '.self::IDC, 'Accept: application/json'), // Format des données
        CURLOPT_SSL_VERIFYPEER  => false,
    ));
  }

  /**
   * Applel d'une liste de critères de recherche
   * @param string $liste
   * @param string [$cache_filename]
   * @param array [$params]
   * @return object[]
   */
  private function GetListe($liste, $cache_filename = null, $params = array()) {
    // Gestion du cache
    //if(!$cache_filename) $cache_filename = $liste.'.json';
    //$cache_file = $this->cache_path.'/'.$cache_filename;

    //if(is_file($cache_file) && (FileMTime($cache_file) >= time() - 86400)) { // On met à jour les données dans le cache s'ils ont plus de 24h
    //  return json_decode(file_get_contents($cache_file));                    // sinon on renvoie les données en cache
    //}

    curl_setopt($this->curl, CURLOPT_URL, self::URL.'liste/'.$liste);
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params ? $params : null);

    if($ret = curl_exec($this->curl)) {
      //file_put_contents($cache_file, $ret);
      return json_decode($ret, true);
    } else {
     // throw new \Exception(curl_error($this->curl), curl_errno($this->curl));
    }
  }

  /**
   * Liste des filères marché
   * @return object[]
   */
  public function Filieres() {
    return $this->GetListe('filieres');
  }

  /**
   * Liste des domaines d'application des aides
   * @return object[]
   */
  public function Domaines() {
    return $this->GetListe('domaines');
  }

  /**
   * Liste des moyens d'intervention des aides
   * @return object[]
   */
  public function Moyens() {
    return $this->GetListe('moyens');
  }

  /**
   * Liste des codes APE de l'INSEE (Nomenclature des Activités Françaises)
   * @return object[]
   */
  public function NAF() {
    return $this->GetListe('naf');
  }

  /**
   * Liste des régions de France
   * @return object[]
   */
  public function Regions() {
    return $this->GetListe('regions');
  }

  /**
   * Liste des départements de France
   * @return object[]
   */
  public function Departements() {
    return $this->GetListe('departements');
  }

  /**
   * Liste des communes d'un dépertement
   * @param string $departement
   * @return object[]
   */
  public function Communes($departement) {
    return $this->GetListe('communes', "communes.$departement.json", array('departement'=>$departement));
  }

  /**
   * Exécution d'une recherche
   * @return boolean|object
   */
  public function Aides() {
    // l'APE ou le SIRET sont obligatoires
    if(!$this->ape && !$this->siret) return false;

    // Le domaine d'application est obligatoire
    if(!$this->domaine) return false;

    // Passage des critères de recherche
    $params = array();
    foreach(array('requete', 'ape', 'domaine', 'moyen', 'filiere', 'departement', 'commune', 'siret') as $k) { $params[$k] = $this->$k; }

    curl_setopt($this->curl, CURLOPT_URL, self::URL.'aides/');
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);

    if($resultat = curl_exec($this->curl)) {
      $resultat = json_decode($resultat);
      $this->requete = $resultat->idr;
      return $resultat;
    } else {
      throw new \Exception(curl_error($this->curl), curl_errno($this->curl));
    }
  }

  /**
   * Chargement de la fiche descriptive d'une aide
   * @param integer $no_aide
   * @return boolean|object
   */
  public function Aide($no_aide) {
    // Le chargement d'une aide dépend de la requête
    if(!$this->requete) return false;

    curl_setopt($this->curl, CURLOPT_URL, self::URL.'aide/');
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, array('requete' => $this->requete, 'dispositif' => $no_aide));

    if($dispositif = curl_exec($this->curl)) {
      return json_decode($dispositif);
    } else {
      throw new \Exception(curl_error($this->curl), curl_errno($this->curl));
    }
  }

    public function listeActivite() {
        $tab =array();
        foreach ($this->NAF() as $ele) {
          $tab[$ele['code']]= $ele['code']." - ".$ele['libelle'];
        }

        return $tab;
    }
     public function listeActiviteCode() {
        $tab =array();
        foreach ($this->NAF() as $ele) {
          $tab[]= $ele['code'];
        }

        return $tab;
    }
     public function GetListeDispo($params = array(), $tab = false) {

    // Passage des critÃ¨res de recherche
    //$params = array();
    //$params = array('ape' => 'A', 'domaine' => '475', 'moyen' => 'Allègement des charges sociales et fiscales');
    //foreach(array('requete', 'ape', 'domaine', 'moyen', 'filiere', 'departement', 'commune', 'siret') as $k) { $params[$k] = $this->$k; }
    curl_setopt($this->curl, CURLOPT_URL, self::URL.'aides/');
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);

    if($resultat = curl_exec($this->curl)) {
        if($tab) {
         //  var_dump(json_decode($resultat));die;
            return json_decode($resultat,true);
        } else {
      $resultat = json_decode($resultat);
      $this->requete = $resultat->idr;
      //dump($resultat);die;
      return $resultat;
        }
    } else {
        $resultat = json_encode(array('resultat' => 'NOK'));
     
    }
  }
  public function AffichageAide($no_aide) {

    curl_setopt($this->curl, CURLOPT_URL, self::URL.'aide/');
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $no_aide);

    if($dispositif = curl_exec($this->curl)) {

      $resultat = json_decode($dispositif,true);
       return $resultat ;
    } else {
       
      throw new \Exception(curl_error($this->curl), curl_errno($this->curl));
    }
  }
}
