<?php

namespace DataConnectBundle\Service;

use Symfony\Component\DependencyInjection\Container;

class Html2Pdf {

    private $pdf;
    private $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function create($orientation = null, $format = null, $lang = null, $unicode = null, $encoding = null, $margin = null) {
        $this->pdf = new \HTML2PDF(
                $orientation ? $orientation : $this->orientation, $format ? $format : $this->format, $lang ? $lang : $this->lang, $unicode ? $unicode : $this->unicode, $encoding ? $encoding : $this->encoding, $margin ? $margin : $this->margin
        );
    }

    public function generatePdf($template, $name, $iso, $y, $m, $user, $userId) {
        chdir($this->container->getParameter('folderPDF'));
        if (is_dir($iso)) {
            chdir($iso);
        } else {
            mkdir($iso);
            chdir($iso);
        }

        if (is_dir($y)) {
            chdir($y);
        } else {
            mkdir($y);
            chdir($y);
        }

        if (is_dir($m)) {
            chdir($m);
        } else {
            mkdir($m, 0777, true);
            chdir($m);
        }
        if (is_dir($user)) {
            chdir($user);
        } else {
            mkdir($user, 0777, true);
            chdir($user);
        }
		// create folder by id of current user session
	   if (is_dir($userId)) {
            chdir($userId);
        } else {
            mkdir($userId);
            chdir($userId);
        }
		
        $this->pdf->writeHtml($template);
        $this->pdf->Output($this->container->getParameter('folderPDF') . $iso . '/' . $y . '/' . $m . '/' . $user . '/'. $userId . '/' . $name . '.pdf', 'F');
        //$this->pdf->Output('', 'D');
    }

}
