<?php
namespace DataConnectBundle\Service;
use DataConnectBundle\Entity\LogFile;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LogService
 *
 * @author hp
 */
class LogService {
    protected  $entityManager;
     protected $context;

    public function __construct( $entityManager,$context)
    {
        $this->entityManager= $entityManager;
        $this->context = $context;
    }
    public function saveLog($code,$url,$username,$password, $arr_msg = array())
    {
       // dump($this->context->getToken()->getUser());die();
        $manager= $this->entityManager;
       // $this->logger->info("click to Search form ".$this->context->getToken()->getUser()->getUsername());
        $logFile= new LogFile();
        //$logFile->setUser($this->context->getToken()->getUser()->getUsername());
        $logFile->setUser($username);
        $logFile->setDate(new \DateTime());
        $logFile->setCodeRetour($code);
        
        if($code == '403'){
            $type = 'Connexion Non aboutie à ONEUP';
            $description= 'curl ' .$url.'transactions -u "'.$username.':'.$password.'"' ;
        }
        else if($code == '200') { 
            if(!empty($arr_msg))
            {
                $type = $arr_msg['type'];
                $description = $arr_msg['message'];
                
            } else {
                  $type = 'Connexion réussite à ONEUP' ;
                  $description= 'curl ' .$url.'transactions -u "'.$username.':'.$password.'"' ; 
            }
            
        }else  { 
            $type ='Echec de connexion à DATA CONNECT ' ;
            $description= 'le login et le mot de passe sont incorrects' ;
        }
    $logFile->setType($type);
    $logFile->setDescription($description);
        
        
       // dump($logFile->setCodeRetour($code));die();
        $manager->persist($logFile);
        $manager->flush();
        
        }
        public function saveLogAttachment($date, $fichier)
    {
       // dump($this->context->getToken()->getUser());die();
        
        $this->logger->info($this->context->getToken()->getUser()->getUsername()." a téléchargé le fichier".$fichier." date: ".$date);
         
    
        }
        
}
