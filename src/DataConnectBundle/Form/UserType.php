<?php

namespace DataConnectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use DataConnectBundle\Form\CompanyType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

class UserType extends AbstractType {
    private $user;
    function __construct($user = null) {
        $this->user = $user;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $setData = 'ROLE_USER';
        if($this->user != null && in_array('ROLE_ADMIN', $this->user->getRoles()))
        {
            $setData = 'ROLE_ADMIN';
        }
            
        $builder
                ->add('login', TextType::class, array('required' => true, 'mapped'=>false))
                ->add('email', EmailType::class, array('required' => true))
                ->add('password', PasswordType::class, array('required' => true))
                ->add('role', ChoiceType::class, array(
                    'choices' => array(
                        'ROLE_USER' => 'User',
                        'ROLE_ADMIN' => 'Admin'),
                    'data' => $setData,
                    'placeholder' => 'Choisissez le role',
                    'mapped'=>false))
                ->add('save', SubmitType::class, array('label' => 'Envoyer'))
                ->add('company', CollectionType::class, array('label'=>false,
                            'entry_type' => CompanyType::class,
                            'allow_add'    => true,
                            'by_reference' => false,
                            'allow_delete' => true,
                        ));        
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'formation\utlisateurBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'dataconnectbundle_user_type';
    }

}
