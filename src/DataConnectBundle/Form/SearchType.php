<?php

namespace DataConnectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SearchType extends AbstractType {

    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        /* $arr = array();
                foreach($this->user as $k=>$v)
                    $arr[$this->user[$k]->getId()] = $this->user[$k]->getCodeCompany();*/

       // $builder->add('user', TextType::class, array('required' => false));
              /*  ->add('user', ChoiceType::class, array(
                            'placeholder' => 'Choisir votre companie',
                            'choices'=>$arr,))*/
        $builder->add('code_folder', ChoiceType::class, array(
                                                    'required'    => true
                                                ));
                         
        $builder->add('month', TextType::class);
        $builder->add('save', SubmitType::class, array('label' => 'Envoyer'));
         
        $builder->add('api', ChoiceType::class, array(
            'choices' => array(
                'Preprod' => 'Staging Api ',
                'Prod' => 'Production Api '),
            'placeholder' => 'Choisissez votre API'));
        
        $builder->add('writingType', ChoiceType::class, array(
            'choices' => array(
                'realized' => 'realized', 
                'forecast' => 'forecast', 
                'closed' => 'closed',
                'draft' => 'draft'),
                'data' => 'realized'));        
      
     //   $builder->add('userApi', TextType::class, array('required' => false))
        $builder->add('cle', TextType::class, array('required' => false));
//         ->add('ftp', TextType::class,array( 'required' => true))
//                 ->add('ftpuser', TextType::class,array( 'required' => true))
//                 ->add('ftppw', TextType::class,array( 'required' => true));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'role' => ['ROLE_USER']
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'dataconnectbundle_search';
    }

}
