<?php

namespace DataConnectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CompanyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('haveCodeCompany', ChoiceType::class, array(
                                        'attr' => array('class'=>'itemRadio'),
                                        'label' => 'Avez vous le code Company : ',
                                        'choices' => array(
                                            '1' => 'Oui',
                                            '0' => 'Non'),
                                        'expanded'=>true))
                ->add('codeCompany', null, array('label' => 'Code Company',
                                                 'attr' => array('class'=>'form-control'),
                                                   'required'=>false))
                ->add('userApi', null, array('label' => 'User Api',
                                                 'attr' => array('class'=>'form-control'),
                                                   'required'=>false))
                ->add('apiKey', null, array('label' => 'Clef Api',
                                                 'attr' => array('class'=>'form-control'),
                                                   'required'=>false))
                ->add('codeFolder', null, array('label' => 'Code Dossier',
                                                'attr' => array('class'=>'form-control',
                                                                'pattern'=>'[a-zA-Z0-9]{6}',
                                                                'oninvalid'=>"setCustomValidity('Le code dossier doit contenir 6 caractères alphanumérique'); loadErrorFunction(this);",
                                                                'onchange'=>"setCustomValidity('');loadSuccessFunction(this);",
                                                                'onkeyup'=>"setCustomValidity('');loadSuccessFunction(this);"),
                                                   'required'=>false))
                ->add('liveMode', ChoiceType::class, array(
                                    'label' => 'Mode Api ',
                                    'attr' => array('class'=>'form-control'),
                                    'choices' => array(
                                        'Preprod' => 'Staging Api ',
                                        'Prod' => 'Production Api '),
                                    'placeholder' => 'Choisissez votre API'));                
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DataConnectBundle\Entity\Company'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dataconnectbundle_company';
    }


}
