<?php
namespace DataConnectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DataConnectBundle\Form\SearchType;
use DataConnectBundle\Form\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use phpseclib\Net\SFTP;
use DataConnectBundle\Entity\LogFile;
use iio\libmergepdf\Merger;
use Symfony\Component\Finder\Finder;
use iio\libmergepdf\Pages;
use ZipArchive;
use DirectoryIterator;
use formation\utlisateurBundle\Entity\User;


use DataConnectBundle\Service\LogService;
//use JMS\SecurityExtraBundle\Annotation\Secure;

class DefaultController extends Controller {

    private $curl = null;
    private $user = "api_154374_f04c@api.oneup.com:fe6947444e1411ba2a71e9ee0d73a0b14c5b351b";

    const URL = 'https://erp-api.fabereo.com/';
    const IDC = '26cf1649045750c76b9328dedad76a821662f9e6';

    public function __construct($cache_path = './') {


        // Création et configuration de la resource CURL
        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_POST => true, // Méthode POST
            CURLOPT_RETURNTRANSFER => true, // On récupère directement les données
            CURLOPT_FAILONERROR => false, // Pas de données s'il y a un code d'erreur HTTP
            CURLOPT_ENCODING => 'gzip', // Compression gzip
            CURLOPT_USERAGENT => 'Mozilla/5.0 (' . trim(`uname -ms`) . '; PHP ' . PHP_VERSION . ') ', // Agent
            CURLOPT_HTTPHEADER => array('X-IDC: ' . self::IDC, 'Accept: application/json'), // Format des données
            CURLOPT_SSL_VERIFYPEER => false,
        ));
    }

//    /**
//     * @Secure(roles="ROLE_USER")
//     */
    public function indexAction(Request $request) {
		
        // to create folder by current user
        $c_userSess = $this->get('security.context')->getToken()->getUser();        
        
        $logger = $this->get('dataconnectbundle_log_file');
        $searchForm = $this->createForm(new SearchType());
        $searchForm->handleRequest($request); //recup donnes
        $monthAPI = ''; //récupération du numéro de mois à utiliser dans l'arborescence de json
        $yy = '';
        $jj = '';
        $hostFtp = 'sftp.sd5.gpaas.net';
        $userFtp = '29726';
        $passFtp = '+DjSx2N\'Z!>3q:p-';
        $parentFolder = 'lamp0/web/vhosts/dataconnect.pmeti.hosting/htdocs/dataconnect/web/bundles/export/';
        $iso = 'FR';
        if ($searchForm->isSubmitted()) {
		
	$info_company_code = $this->getCodeCompanyBelongFolder((int)$request->request->get('dataconnectbundle_search')['code_folder']);	
	
        if(in_array('ROLE_ADMIN', $c_userSess->getRoles()) == true)
	{
		$apiKey = $searchForm['api']->getData();
	} 
	else
	{
                $apiKey = $info_company_code['live_mode'];
	}
		$user = $info_company_code['code_company'];
		$api = $apiKey;
		$userApi = $info_company_code['user_api'];

                $clef = $info_company_code['api_key'];
                $question = ($info_company_code['have_code_company'] == true)? 'non':'oui';

		if ($api == 'Preprod') {
			if ($question == 'non') {
				$username = 'api_fabereo_' . $user . '@erp-api.fabereo.com';
				$s = hash_hmac('sha256', $username, 'wJwJP$F@U55c5Zx-', true);
				$password = base64_encode($s);
			} else {
				$username = $userApi;
				$password = $clef;
				$user = strstr($username, '@', true);
			}
			$url = 'https://stagingapi.oneup.com/v1/';
			$urlAttach = "https://staging.oneup.com/attachments/";
		} else if ($api == 'Prod') {
			if ($question == 'non') {
				$username = 'api_fabereo_' . $user . '@erp-api.fabereo.com';
				$s = hash_hmac('sha256', $username, '1siH23sas872b1!sad-*$dwqsh830h', true);
				$password = base64_encode($s);
			} else {
				$username = $userApi;
				$password = $clef;
				$user = strstr($username, '@', true);
			}
			$url = 'https://erp-api.fabereo.com/v1/';
			$urlAttach = "https://erp.fabereo.com/attachments/";
		}
                //The offset of the first record returned
                $offset = 0;
                $transactions = array('result'=>array(),'code'=>'');
                $month = $searchForm['month']->getData();
		$pushTransactions = $this->getTransaction($url, $username, $password, $offset,$month); //afficher le tableau contenant la liste des transactions  
             
                if(empty($pushTransactions['result']))
                {
                    $transactions['result'] = $pushTransactions['result'];
                    $transactions['code'] = $pushTransactions['code'];
                }
                else
                {    
                        /*** START get aLL record by offset ***/
                        while (!empty($pushTransactions['result'])) {                                
                            $transactions['result'] = array_merge($transactions['result'], $pushTransactions['result']);
                            $transactions['code'] = $pushTransactions['code'];
                            $offset  = $offset + 100;
                            $pushTransactions = $this->getTransaction($url, $username, $password, $offset,$month);
                            
                        }
                        unset($offset, $pushTransactions);
                        /*** END get All record By offset ***/
                }
              
		$logger->saveLog($transactions['code'], $url, $username, $password);
		$month = $searchForm['month']->getData();

		$monthAPI = substr($month, 5, 2); //récupération du numéro de mois à utiliser dans l'arborescence de json
		$yy = substr($month, 0, 4); //récupération de l'année à utiliser dans l'arborescence de json
		$journeaux = array(); //mois
		$q = '';
		$attachments = array();
                $attachments_img = array();
                
                //  type d'écriture 
                $writingType = $searchForm['writingType']->getData();

        if (empty($transactions['result'])) { //si le code company est invalide cad si le resultat retourné des transactions est vide
                return $this->render('DataConnectBundle:Default:index.html.twig', array(
                                        'response' => '',
                                        'attachments' => '',
                                        'piece_number' => '',
                                        'message' => 'code company non valide',
                                        'searchForm' => $searchForm->createView(),
                                        'mois' => '',
                                        'year' => ''
                ));
            } else {

                $exist_attchements = false;
                $responseCSV = "";
                $namePdf = "";
                $fileZip = "";
                $isExistJsonAttachement = false; // pour véirifier si tt les json d'attachement sont vide, pour crée path de fichier FR/2017/iso...ETC
               $logService = new LogService($this->getDoctrine()->getManager(), $this->get('security.context'));
                
                foreach ($transactions['result'] as $key => $value) {
                    $m = substr($value['date'], 5, 2);
                    $y = substr($value['date'], 0, 4);
                    $monthApi = $y . '-' . $m;
                    
                    $listAttach = [];
                    $listAttachImg = [];
                    $listAttachPdf = [];
                    
                       
                    if (($month == $monthApi) && ($writingType == $value['status'])) 
                    {    
                     
                        $journeaux[] = $value;
                        $chemin = $this->getParameter('folderPDF'). $iso . '/' . $y . '/' . $m . '/' . $user .'/'.$info_company_code['code_folder'];
                        $q = $value['piece_number']; //récupérer le piece_number pour l'endpoint de téléchargement des piéces jointes.
                        $data_att = $this->getAttachments($url, $q, $username, $password);
                        $exist_attchements = true;
                        if(!empty($data_att))
                        {
                            $isExistJsonAttachement = true; // si existe des attachements pour fa   ire parse
                            foreach ($data_att as $key => $value) // start $data_att
                            {       
                              if($value['attached_to']['user_code'] == $q) // start check user_code_trans == user_code_att                                    
                                 {
                                  
                                      $attachments[] = $value;
                                      $last_error_file = $this->isTrueFile($value['public_key']);
                                   
                                      // vérifier si fichier exist ou bien il y a erreur
                                      if($last_error_file['code'] == false)
                                      {
                                          $logService->saveLog('200', '', $username, $password, array('type'=>'Erreur fichier', 'message'=>$last_error_file['message']));
                                          continue;
                                      }
                                      // pour exclus type application/octet-stream
                                      else if($value['mime_type'] === "application/octet-stream" )
                                      {
                                          $logService->saveLog('200', '', $username, $password, array('type'=>'Impossible de charger l\'image ', 
                                              'message'=>'ERREUR : Impossible de charger l\'image https://erp.fabereo.com/attachments/'.$value['public_key'] ));
                                          continue;
                                      }
                                      else
                                      {
                                        if ($this->isBelongExcludeMimetypesFileType($value['mime_type'])){
                                             // dump($q);die();
                                              $listAttachPdf[] = $value;
                                            //  dump('2');
                                         }else if (($value['mime_type'] !== "application/pdf")) {
                                              $listAttachImg[] = $value;
                                              //dump($value['mime_type']);
                                          }
                                      }
                                        
                                     
                                 } // end check user_code_trans == user_code_att
                                    
                                 
                            
                            }// end $data_att
//                            dump($listAttachPdf);
//                              dump($listAttachImg);
//die('hello');
                            $namePdf = $this->pdfConvert($listAttachImg, $iso, $y, $m, $user, $info_company_code['code_folder']);
                          //  $chemin = $this->getParameter('folderPDF'). $iso . '/' . $y . '/' . $m . '/' . $user .'/'.$info_company_code['code_folder'];

                            foreach ($listAttachPdf as $onePdf) {

                                file_put_contents(
                                        $chemin . '/' . $onePdf['name'], file_get_contents('https://erp.fabereo.com/attachments/' . $onePdf['public_key'])
                                );
                            }

                            $merge = new Merger();
            //add pdf file
                            
                            foreach ($listAttachPdf as $k => $onePdf) {  
                                if($this->isBelongExcludeMimetypesFileType($onePdf['mime_type'], array('application/pdf')))
                                {
                                    // save log if format file DOCX                                    
                                    $logService->saveLog('200','',$username,$password, array(
                                        'type'=> 'Document invalide',
                                        'message'=> 'Document format invalid, merge est impossible ('.$onePdf['name'].')'
                                    ));
                                    
                                    unset($listAttachPdf[$k]);
                                    continue; // pass to next steps
                                }
                                
                                if($this->_isMergeOk($chemin . '/' . $onePdf['name']) == true)
                                {
                                    $merge->addFromFile($chemin . '/' . $onePdf['name']);
                                }
                                else
                                {
                                    // Save log if file PDF is Protected
                                    $logService = new LogService($this->getDoctrine()->getManager(), $this->get('security.context'));
                                    $logService->saveLog('200','',$username,$password, array(
                                        'type'=> 'PDF protégé',
                                        'message'=> 'Document PDF protégé, merge est impossible ('.$onePdf['name'].')'
                                    ));
                                    unset($listAttachPdf[$k]);
                                }
                            }
            //add img pdf

                            $merge->addFromFile($chemin . '/' . $namePdf . '.pdf');

                            if(!empty($listAttachPdf) || !empty($listAttachImg)) //if number of file to merge more than 2
                            {
                                 file_put_contents($chemin . '/' . $iso . '_' . $y . '_' . $m . '_' . $user . '_'. $info_company_code['code_folder'].'_'. $q . '.pdf', $merge->merge());
                            }
                               

                            @unlink($chemin . '/' . $namePdf . '.pdf');

                            foreach ($listAttachPdf as $onePdf) {
                                @unlink($chemin . '/' . $onePdf['name']);
                            }                                                      
                        } 
                        
//                        if(strpos($value['piece_number'], 'ENC-') !== false)
//                        {
//                             $attachments_img = $attachments;
//                        }
                       // dump($attachments);
                        //$listAttach[$value['piece_number']] = $attachments;
                        ///test pour la convertion en pdf des piéces jointes
                        /* foreach ($attachments as $key=> $value){
                          if ($value['mime_type'] == "application/pdf"){
                          }
                          else{
                          dump($pdf1 = $this->pdfAction($value));
                          }

                          } */
                    } else {
                        $m = $monthAPI;
                        $y = $yy;
                    }
                }
           
                if(!$isExistJsonAttachement && !empty($journeaux)) // on va crée les dossier
                {
                    $this->applayToCreateFolder($iso, $y, $m, $user, $info_company_code['code_folder']);
                }
             
                $sftp = $this->connectToServer($hostFtp, $userFtp, $passFtp);
                $responseCSV = $this->exportCSV($sftp, $parentFolder, $y, $m, $user, $journeaux, $attachments, $urlAttach, $q, $info_company_code['code_folder']); //pour exporter un fichier csv
               if($exist_attchements)
               {
                   
                    $files = scandir($this->getParameter('folderPDF'));

                    foreach ($files as $entry) {

                        if ($entry != "." && $entry != ".." && is_file($entry)) {
                            unlink($this->getParameter('folderPDF') . $entry);
                        }
                    }

                /** ZIP FILES BY MONTH ***/
                    $source_dir = $chemin;				
                    $zip_file_name =   $iso. '_' . $y . '_' . $m . '_' . $user . '_' . $info_company_code['code_folder'].'.zip';
                    if(file_exists ($source_dir.'/'.$zip_file_name))
                            unlink($source_dir.'/'.$zip_file_name);
                    $fileZip = $this->zipFiles($source_dir, $zip_file_name, $iso, $y, $m, $user, $info_company_code['code_folder']);

                /** delete FILES without zip folder ***/
                    $files = array_diff(glob($source_dir."/*.*"), glob($source_dir."/*.zip"));
                    foreach($files as $file) {
                      if(is_file($file))
                        unlink($file); // delete file
                    } 
                   
                /****** SFTP upload zip ******/
                $this->createFolder($sftp, $parentFolder, $iso, $y, $m, $user, $info_company_code['code_folder']);                
                $c = file_get_contents($fileZip['full_path']);
                $sftp->put($zip_file_name, $c);
                
               }   
               else
               {
                   $message = "Aucune transaction n'a été trouvée";
               }
               
                return $this->render('DataConnectBundle:Default:index.html.twig', array(
                            'response' => $responseCSV,
                            //  'response' => $response,
                            'attachments' => $attachments,
                            'piece_number' => $q,
                            'message' => ((isset($message))? $message:''),
                            'searchForm' => $searchForm->createView(),
                            'mois' => $m,
                            'year' => $y,
                            'namePdf' => $namePdf,
			    'fileZip' => ((isset($fileZip['download_path']))? $fileZip['download_path']: ''),
                ));
            }
        }//end submit
        return $this->render('DataConnectBundle:Default:index.html.twig', array(
                    'searchForm' => $searchForm->createView(),
                    'mois' => $monthAPI,
                    'year' => $yy,
                    'jour' => $jj
        ));
    }

    public function exportCSV($sftp = null, $parentFolder, $y, $m, $user, $journeaux, $attachments, $urlAttach, $q, $code_folder) {

         $response = new StreamedResponse();
        $filepath = '';
        $iso = 'FR';        
        $nom_file = $iso . '_' . $y . '_' . $m . '_' . $user . '_'. $code_folder . '_' . time() . '.csv';
        $filepath = 'dataconnect/web/bundles/export/' . $iso . '/' . $y . '/' . $m . '/' . $user . '/'. $code_folder . '/' . $nom_file;        
        $handle = fopen($nom_file, 'w');

        // Add the header of the CSV file
        fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF)); //pour l'encodage utf8
        fputcsv($handle, array('code', 'Id', 'Piece_number', 'Status', 'Date', 'label', 'Account label',
            'way', 'amount', 'currency_iso_code'), ';');

        foreach ($journeaux as $key => $row) {
            foreach ($row['entries'] as $key => $entry) {
                fputcsv(
                        $handle, // The file pointer
                        array($row['journal']['code'],
                    $row['id'],
                    $row['piece_number'],
                    $row['status'],
                    $row['date'],
                    $entry['label'],
                    $entry['account_label'],
                    $entry['way'],
                    $entry['amount'],
                    $row['currency_iso_code'],
                        ), // The fields
                        ';' // The delimiter
                );
            }
        }


        fclose($handle);
        $filecontent = file_get_contents($nom_file);

        $filename = $iso . '_' . $y . '_' . $m . '_' . $user . '_' . time() . '.csv';
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $filename);

        return array($filename, $filepath);
        // return $response;
    }

    public function connectToServer($hostFtp, $userFtp, $passFtp) {

        $sftp = new SFTP($hostFtp);
        if (!$sftp->login($userFtp, $passFtp)) {
            throw new \Exception('Cannot login into your server !');
        }

        return $sftp;
    }

    public function createFolder($sftp, $parentFolder, $iso, $y, $m, $user, $userId) {   
       $sftp->chdir($parentFolder); //création du dossier distant qui contiendra tous les journaux
       $sftp->mkdir($iso);
       $sftp->mkdir($iso . '/' . $y);
       $sftp->mkdir($iso . '/' . $y . '/' . $m);
       $sftp->mkdir($iso . '/' . $y . '/' . $m . '/' . $user);
       $sftp->mkdir($iso . '/' . $y . '/' . $m . '/' . $user . '/' . $userId);
       $sftp->chdir($iso . '/' . $y . '/' . $m . '/' . $user . '/'. $userId . '/');

        return $sftp;
    }

    public function exportJSON($sftp, $parentFolder, $attachments, $urlAttach, $q, $y, $m, $user, $journeaux) {
        ini_set("allow_url_fopen", 1);
        $response = new StreamedResponse();
        $filepath = '';
        $iso = 'FR';

        $nom_file = $iso . '_' . $y . '_' . $m . '_' . $user . '_' . time() . '.json';
        $filepath = 'dataconnect/web/bundles/export/' . $iso . '/' . $y . '/' . $m . '/' . $user . '/' . $nom_file;
        $sftp->put($nom_file, $journeaux);

        foreach ($attachments as $key => $row) {
           
            $imageString = file_get_contents($urlAttach . $row['public_key']);
            $sftp->put($iso . '_' . $y . '_' . $m . '_' . $user . '_attachment_' . $q . '_' . $row['name'], $imageString);
        }
        return array($nom_file, $filepath);
        //return($nom_file);
    }

    public function getTransaction($url, $username, $password, $offset,$date) {

        $trans = "transactions";
        $offset = '?limit=100&offset='.$offset;
        $firstDay = $date.'-01';
        $lastDay  = date("Y-m-t", strtotime($firstDay));
        $date = '&date=gte:'.$firstDay.'%20AND%20lte:'.$lastDay;
        /*   code CURL   */

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url . $trans. $offset.$date);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //code copié
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        ///
        $result = curl_exec($curl);   //cette ligne affiche la liste de tous les journaux
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($curl);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($result));
        //dump($http_status);die();
        if ($http_status == 401) {
            echo "HTTP Status == 401 <br/>";
            echo "Curl Errno returned $curl_errno <br/>";
        } else {
            // ferme la ressource cURL et libère les ressources systèmes
            //    dump(json_decode($result, true));die();
            //return json_decode($result, true);
            return array('result' => json_decode($result, true), 'code' => $http_status);
        }
    }

    public function getAttachments($url, $q, $username, $password) {
        //  $url = "https://api.oneup.com/v1/attachments";
        // $url = "https://erp-api.fabereo.com/v1/attachments"; // production api host
        $attach = "attachments";
        /*   code CURL   */
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url . $attach.'?q={'.$q.'}');
       // curl_setopt($this->curl, CURLOPT_POSTFIELDS, $q);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);

        $result = curl_exec($curl);   //cette ligne affiche la liste de tous les attachements
        //dump($result);die();
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($curl);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($result));
        if ($http_status == 401) {

            echo "HTTP Status == 401 <br/>";
            echo "Curl Errno returned $curl_errno <br/>";
        } else {
            // ferme la ressource cURL et libère les ressources systèmes
            return json_decode($result, true);
        }
    }


    public function listerAction() {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $rep = $em->getRepository("DataConnectBundle:LogFile");

        $logs = $rep->findAll();
        //  dump($logs); die();
        return $this->render('DataConnectBundle:Default:lister.html.twig', array('logs' => $logs));
    }

    public function addUserAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $userForm = $this->createForm(new UserType());
        $userForm->handleRequest($request);
        
        if ($userForm->isSubmitted()) {
            $login = $userForm['login']->getData();
            $email = $userForm['email']->getData();
            $password = $userForm['password']->getData();
            $role = $userForm['role']->getData();
            $company = $userForm['company']->getData();
            
      
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setUsername($login);
            $user->setEmail($email);
            $user->setRoles([$role]);
           
            $user->setPlainPassword($password);
            $user->setEnabled(true);
           // var_dump($company);die();
            foreach($company as $c )            
                 $user->addCompany($c);
            
            $loginExist = $userManager->findUserBy(array('username' => $login));
            if(is_object($loginExist))
            {
                     $this->addFlash('error', 'Un utilisateur avec ce login existe déjà');
            }else
            {
                 $emailExist = $userManager->findUserBy(array('email' => $email));
                if(is_object($emailExist))
                {
                     $this->addFlash('error', 'Un utilisateur avec cet email existe déjà');
                }else
                {   
                    try {
                        $userManager->updateUser($user);
                        $this->getDoctrine()->getManager()->flush();                               
                    } catch(\Exception $e){
                          $this->addFlash(
                               'error', 'Erreur inconnue lors du processus d\'enregistrement');
                         return $this->redirectToRoute('data_connect_add_user'); 
                     }
                     $this->addFlash('notice', 'Utilisateur crée avec succès');                  
                }
            }
            return $this->redirectToRoute('data_connect_add_user');
            
        }
        return $this->render('DataConnectBundle:Default:newUser.html.twig', array(
                    'userForm' => $userForm->createView(),
        ));
    }

    public function listerUserAction() {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();

        return $this->render('DataConnectBundle:Default:listerUser.html.twig', array('users' => $users));
    }

    public function pdfAction($attachments) {

        $template = $this->renderView('DataConnectBundle:Default:pdf.html.twig', [
            'attachments' => $attachments,
            'remise' => 25,
            'tva' => 19.6,
            'frais_de_port' => 10
        ]);
        $html2pdf = $this->get('app.html2pdf');
        $html2pdf->create('P', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
        return $html2pdf->generatePdf($template, "facture");
    }

    public function pdfConvert($attachments, $iso, $y, $m, $user, $userId) {
        $template = $this->renderView('DataConnectBundle:Default:pdf.html.twig', [
            'attachments' => $attachments,
            'remise' => 25,
            'tva' => 19.6,
            'frais_de_port' => 10
        ]);
        $html2pdf = $this->get('app.html2pdf');
        $html2pdf->create('P', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
        $name = "facture" . time();
        $html2pdf->generatePdf($template, $name, $iso, $y, $m, $user, $userId);
        return $name;
    }
	
    public function treeFileAction()
    {		
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
                return $this->redirect( $this->generateUrl('data_connect_homepage') );
        }
       $files =  $this->dir2json($this->getParameter('web_dir').'bundles/export/');	   
			
       return $this->render('DataConnectBundle:Default:tree_files.html.twig', array('files'=>$files));
    }
	
    private function zipFiles($source_dir, $zip_file, $iso, $y, $m, $user, $code_folder)
    {
            $full_path = $this->container->getParameter('kernel.root_dir').'/../web/bundles/export/' . $iso . '/' . $y . '/' . $m . '/' . $user . '/'. $code_folder . '/';
            $download_path = 'dataconnect/web/bundles/export/' . $iso . '/' . $y . '/' . $m . '/' . $user . '/'. $code_folder . '/';
            $pdfName =  $iso . '_' . $y . '_' . $m . '_' . $user .'_'.$code_folder.'.pdf';
            $zip = new ZipArchive();
            if ($zip->open($zip_file, ZipArchive::CREATE) === TRUE){

                    foreach (new DirectoryIterator(($source_dir)) as $fileInfo) {
                            if (in_array($fileInfo->getFilename(), [ ".", ".." ])) { continue; }
                            if($this->addFileToByExtension($fileInfo->getExtension()))
                            {
                                $fileName = $fileInfo->getFilename();
                                $zip->addFile($fileName);
                            }
                    }   

                    $zip->close();
                    return array('full_path'=>$full_path.$zip_file, 'download_path'=>$download_path.$zip_file);
            } 
                    return false;
    }
	
    private function folderToZip()
    {
            if ($zipFile == null) {
                            // no resource given, exit
                            return false;
                    }
                    // we check if $folder has a slash at its end, if not, we append one
                    $folder .= end(str_split($folder)) == "/" ? "" : "/";
                    $subfolder .= end(str_split($subfolder)) == "/" ? "" : "/";
                    // we start by going through all files in $folder
                    $handle = opendir($folder);
                    while ($f = readdir($handle)) {
                            if ($f != "." && $f != "..") {
                                    if (is_file($folder . $f)) {
                                            // if we find a file, store it
                                            // if we have a subfolder, store it there
                                            if ($subfolder != null)
                                                    $zipFile->addFile($folder . $f, $subfolder . $f);
                                            else
                                                    $zipFile->addFile($folder . $f);
                                    } elseif (is_dir($folder . $f)) {
                                            // if we find a folder, create a folder in the zip 
                                            $zipFile->addEmptyDir($f);
                                            // and call the function again
                                            $this->folderToZip($folder . $f, $zipFile, $f);
                                    }
                            }
                    }
    }


    public function getFolderAction()
    {
        
        // admin connected
         if ($this->get('security.context')->isGranted('ROLE_ADMIN')) 
         {
             $folders = $this->getAllFolderCompany();
             return new JsonResponse(array(
                    'folders' => $folders,
                ));
         }
         
        $c_userSess = $this->get('security.context')->getToken()->getUser()->getCompany()->getValues();
        $folders = array();
        $i = 0;
        foreach($c_userSess as $k=>$v)
        {
                $folders[$i] = array('id'=>$c_userSess[$k]->getId(),
                                 'name'=>$c_userSess[$k]->getCodeFolder()); 
                $i++;
        }
        return new JsonResponse(array(
            'folders' => $folders,
        ));
    }
    
    private function getCodeCompanyBelongFolder($id)
    {
         $_arr_company = array();
    
        //admin connected
         if ($this->get('security.context')->isGranted('ROLE_ADMIN')) 
         {        
            $folders = $this->getAllCodeCompany();
            foreach($folders as $k=>$v)
            {
                if($id === $folders[$k]['id'])
                {
                    $_arr_company['code_company'] = $folders[$k]['code_company']; 
                    $_arr_company['code_folder'] = $folders[$k]['code_folder']; 
                    $_arr_company['have_code_company'] = $folders[$k]['have_code_company']; 
                    $_arr_company['user_api'] = $folders[$k]['user_api']; 
                    $_arr_company['api_key'] = $folders[$k]['api_key']; 
                    $_arr_company['live_mode'] = $folders[$k]['live_mode']; 
                    break;
                }
            }
         }
         else
         {
              $c_userSess = $this->get('security.context')->getToken()->getUser()->getCompany()->getValues();               
                foreach($c_userSess as $k=>$v)
                {
                    if($id === $c_userSess[$k]->getId())
                    {
                        $_arr_company['code_company'] = $c_userSess[$k]->getCodeCompany(); 
                        $_arr_company['code_folder'] = $c_userSess[$k]->getCodeFolder();                         
                        $_arr_company['have_code_company'] = $c_userSess[$k]->getHaveCodeCompany(); 
                        $_arr_company['user_api'] = $c_userSess[$k]->getUserApi(); 
                        $_arr_company['api_key'] = $c_userSess[$k]->getApiKey(); 
                        $_arr_company['live_mode'] = $c_userSess[$k]->getLiveMode();
                        break;
                    }
                }
         }
           
        
        return $_arr_company;
    }
    
    public function editUserAction(Request $request, User $user) {
        $em = $this->getDoctrine()->getManager();
        $_user = $em->getRepository('utilisateurBundle:User')->find($user);
        $passwd = $_user->getPassword();
        $editForm = $this->createForm(new UserType($_user), $_user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            
             $login = $editForm['login']->getData();
            $email = $editForm['email']->getData();
            $role = $editForm['role']->getData();
            $company = $editForm['company']->getData();
            
            $_user->setUsername($login);
            $_user->setEmail($email);
            $_user->setRoles([$role]);            
            $_user->setPassword($passwd);
            
            /*********************/
            $userManager = $this->get('fos_user.user_manager');
            $loginExist = $userManager->findUserBy(array('username' => $login));

            if(is_object($loginExist) && ($loginExist->getId() != $_user->getId()))
            {
                     $this->addFlash('error', 'Ce login est déjà utilisé');
            }else
            {
                 $emailExist = $userManager->findUserBy(array('email' => $email));
                if(is_object($emailExist) && ($emailExist->getId() != $_user->getId()))
                {
                     $this->addFlash('error', 'Cette adresse mail est déjà utilisée');
                }else
                {   
                    try {
                       $this->getDoctrine()->getManager()->flush();                              
                    } catch(\Exception $e){
                          $this->addFlash(
                               'error', 'Erreur inconnue lors du processus d\'enregistrement');
                        return $this->redirectToRoute('data_connect_edit_user', array('id' => $_user->getId())); 
                     }
                     $this->addFlash('notice', 'Compte mis à jour avec succès');                  
                }
            }
            /*********************/
            

            return $this->redirectToRoute('data_connect_edit_user', array('id' => $_user->getId()));
        }

        return $this->render('DataConnectBundle:Default:editUser.html.twig', array(
                    'user' => $_user,
                    'userForm' => $editForm->createView(),
        ));
    }
    
    private function getAllFolderCompany()
    {
        $em = $this->getDoctrine()->getManager();
        $_users = $em->getRepository('utilisateurBundle:User')->findAll();
        $folders = array();
        $i = 0;
        foreach($_users as $k => $_v)
        {
            $data = $_v->getCompany()->getValues();
            foreach($data as $k => $v)
            {
                 $folders[$i] = array('id_user'=>$_v->getId(),
                                      'id'=>$data[$k]->getId(),
                                      'name'=>$data[$k]->getCodeFolder()); 
                  $i++;
            }
        }
        return $folders;
    }
    
    private function getAllCodeCompany()
    {
        $em = $this->getDoctrine()->getManager();
        $_users = $em->getRepository('utilisateurBundle:User')->findAll();
        $folders = array();
        $i = 0;
        foreach($_users as $k => $v)
        {
            $data = $v->getCompany()->getValues();
            foreach($data as $k => $v)
            {
                 $folders[$i] = array('id'=>$data[$k]->getId(),
                                 'have_code_company'=>$data[$k]->getHaveCodeCompany(),
                                 'code_folder'=>$data[$k]->getCodeFolder(),
                                 'code_company'=>$data[$k]->getCodeCompany(),                                 
                                 'user_api'=>$data[$k]->getUserApi(),
                                 'api_key'=>$data[$k]->getApiKey(),
                                 'live_mode'=>$data[$k]->getLiveMode()); 
                  $i++;
            }
        }
        return $folders;
    }
    
    public function existFolderAction($id, $code_folder)
    {
       $folders = array_values($this->getAllFolderCompany());
       foreach($folders as $k => $v)
       {
           if($v['name'] == $code_folder && ($id != $v['id_user']))
           {
               return new JsonResponse(array(
                    'existed' => true,
                ));
           }
       }
        return new JsonResponse(array(
                    'existed' => false,
                ));
          
    }
    
  private function is_dir_empty($dir) {
    if (!is_readable($dir)) return NULL; 
    return (count(scandir($dir)) == 2);
  }
  
  // check if file is protected
  private function _isMergeOk($fileName) {
      $merge = new Merger();
      $merge->addFromFile($fileName);
      $merge->addFromFile($this->container->getParameter('kernel.root_dir').'/../web/uploads/images/sample.pdf');      
      if($merge->merge() === 'KO')
      {
          // protected
          return false;
      }
      // free
      return true;
  }
  
  private  function applayToCreateFolder($iso, $y, $m, $user,$code_folder)
  {
       chdir($this->container->getParameter('folderpdf'));
        if (is_dir($iso)) {
            chdir($iso);
        } else {
            mkdir($iso);
            chdir($iso);
        }

        if (is_dir($y)) {
            chdir($y);
        } else {
            mkdir($y);
            chdir($y);
        }

        if (is_dir($m)) {
            chdir($m);
        } else {
            mkdir($m, 0777, true);
            chdir($m);
        }
        if (is_dir($user)) {
            chdir($user);
        } else {
            mkdir($user, 0777, true);
            chdir($user);
        }
		// create folder by id of current user session
	   if (is_dir($code_folder)) {
            chdir($code_folder);
        } else {
            mkdir($code_folder);
            chdir($code_folder);
        }
  }
  
  private function isTrueFile($key)
  {
      $str = @file_get_contents('https://erp.fabereo.com/attachments/'.$key);
      if ($str === FALSE) {
        $error = error_get_last();
        return array('code'=>false, 'message'=>'Échec d\'ouverture le fichier: la demande HTTP a échoué Erreur du serveur HTTP / 1.1 500');
      }
      return array('code'=>true, 'message'=>1);    
  }
  
  private function isBelongExcludeMimetypesFileType($mimeType, $_excludeArray = NULL){
      $_arr = array(
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/msword',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/pdf');
      if( ! is_null($_excludeArray))
      {
          foreach ($_excludeArray as $v)
          {
              unset($_arr[$v]);
          }
      }
      return in_array($mimeType, $_arr);
  }
  
  private function addFileToByExtension($_extension){
      return in_array($_extension, array('csv', 'pdf', 'docx', 'doc', 'xlsx', 'xls') );
  }
}
