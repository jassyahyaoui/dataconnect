<?php

namespace DataConnectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="DataConnectBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="have_code_company", type="boolean")
     */
    private $haveCodeCompany;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code_company", type="string", length=255, nullable=true)
     */
    private $codeCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="code_folder", type="string", length=255, nullable=true)
     */
    private $codeFolder;

    /**
     * @var string
     *
     * @ORM\Column(name="user_api", type="string", length=255, nullable=true)
     */
    private $userApi;
    
    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=255, nullable=true)
     */
    private $apiKey;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="live_mode", type="string", columnDefinition="enum('Preprod', 'Prod')")
     */
    private $liveMode; 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set codeCompany
     *
     * @param string $codeCompany
     * @return Company
     */
    public function setCodeCompany($codeCompany)
    {
        $this->codeCompany = $codeCompany;

        return $this;
    }

    /**
     * Get codeCompany
     *
     * @return string 
     */
    public function getCodeCompany()
    {
        return $this->codeCompany;
    }

    /**
     * Set codeFolder
     *
     * @param string $codeFolder
     * @return Company
     */
    public function setCodeFolder($codeFolder)
    {
        $this->codeFolder = $codeFolder;

        return $this;
    }

    /**
     * Get codeFolder
     *
     * @return string 
     */
    public function getCodeFolder()
    {
        return $this->codeFolder;
    }

    /**
     * Set userApi
     *
     * @param string $userApi
     * @return Company
     */
    public function setUserApi($userApi)
    {
        $this->userApi = $userApi;

        return $this;
    }

    /**
     * Get userApi
     *
     * @return string 
     */
    public function getUserApi()
    {
        return $this->userApi;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     * @return Company
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string 
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set haveCodeCompany
     *
     * @param boolean $haveCodeCompany
     * @return Company
     */
    public function setHaveCodeCompany($haveCodeCompany)
    {
        $this->haveCodeCompany = $haveCodeCompany;

        return $this;
    }

    /**
     * Get haveCodeCompany
     *
     * @return boolean 
     */
    public function getHaveCodeCompany()
    {
        return $this->haveCodeCompany;
    }

    /**
     * Set liveMode
     *
     * @param string $liveMode
     * @return Company
     */
    public function setLiveMode($liveMode)
    {
        $this->liveMode = $liveMode;

        return $this;
    }

    /**
     * Get liveMode
     *
     * @return string 
     */
    public function getLiveMode()
    {
        return $this->liveMode;
    }
}
