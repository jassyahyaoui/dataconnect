<?php
namespace DataConnectBundle\Listener;

use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use FOS\UserBundle\Model\UserManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Response;


class LoginListener
{
    protected $logFile;
    
    public function __construct($logFile = null) {
        
        $this->logFile = $logFile;
    }
   
    public function onAuthenticationFailure( AuthenticationFailureEvent $event )
    {  
        $user =$event->getAuthenticationToken()->getUser();
        $pass =$event->getAuthenticationToken()->getCredentials();
        //dump($event->getAuthenticationToken());die;
        $this->logFile->saveLog('406 da','url login', $user, $pass );
        // executes on failed 
       // $this->get('gggggg')->send()
        
    }
    
    public function onAuthenticationSuccess (InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
       // var_dump($user);die;
        if ($user) {
            // your action with the current user
return new Response();

        }
    }
}