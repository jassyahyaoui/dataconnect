<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use ApiBundle\Services\LesAides;

class SearchType extends AbstractType
{
    private $lesaidesService;

    public function __construct(LesAides $lesaidesService) {
        $this->lesaidesService = $lesaidesService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('departement', ChoiceType::class, array(
                    'choices' => $this->listeDepartement()
                ))
                ->add('commune', TextType::class, array('required' => false))
                ->add('siret', TextType::class, array('required' => false))
                ->add('activity', ChoiceType::class, array(
                        'choices'  => $this->listeActivite(),
                    'placeholder' => 'choisir une activite',
                     /*'choice_attr' => function($val, $key, $index) {

                     $tab = $this->listeActiviteCode();
                    return ['data' =>  $tab[$val]];
                  },*/
                    ))
                ->add('fliliere', ChoiceType::class, array(
                        'choices'  => $this->listeFileiere(),
                        'required' => false,
                        'placeholder' => 'choisir une filiere',
                    ))
                ->add('domaine', ChoiceType::class, array(
                        'choices'  => $this->listeDomaine(),
                    ))
                ->add('moyen', ChoiceType::class, array(
                        'choices'  => $this->listeMoyen(),
                    'required' => false,
                        'placeholder' => 'choisir un moyen',
                    
                    ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_search';
    }

    public function listeFileiere() {
        //dump( array('1' => 'ddd', '77' => 'mm'));die;
        //dump( $this->lesaidesService->Filieres());
        //var_dump($res[0]);
       // die;
        $tab =array();
        foreach ($this->lesaidesService->Filieres() as $ele) {
           $tab[ $ele['numero']]= $ele['libelle'];
        }
        return $tab;
        //return $this->lesaidesService->getfil();
        //return $this->lesaidesService->Filieres();
    }

    public function listeActivite() {

        return  $this->lesaidesService->listeActivite();
    }
    public function listeActiviteCode() {

        return  $this->lesaidesService->listeActiviteCode();
    }
    public function listeDomaine() {

        $tab =array();
        foreach ($this->lesaidesService->Domaines() as $ele) {
          $tab[ $ele['numero']]= $ele['libelle'];
        }

        return $tab;
    }
    public function listeDepartement() {

        $tab =array();
        //dump( $this->lesaidesService->Departements());die();
        foreach ($this->lesaidesService->Departements() as $ele) {
          $tab[ $ele['departement']]= $ele['departement'].'-'.$ele['nom'];
         // $tab[ $ele['nom']]= $ele['departement'].'-'.$tab[ $ele['nom']]= $ele['nom'];
        }

        return $tab;
    }
public function listeMoyen() {

        $tab =array();
        //dump( $this->lesaidesService->Moyens());die();
        foreach ($this->lesaidesService->Moyens() as $ele) {
          $tab[ $ele['numero']]= $ele['libelle'];
        }

        return $tab;
    }

}
