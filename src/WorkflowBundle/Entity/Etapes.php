<?php

namespace WorkflowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Etapes
 *
 * @ORM\Table(name="etapes")
 * @ORM\Entity(repositoryClass="WorkflowBundle\Repository\EtapesRepository")
 */
class Etapes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="numEtape", type="string", length=255, nullable=true)
     */
    private $numEtape;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Etapes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set numEtape
     *
     * @param string $numEtape
     * @return Etapes
     */
    public function setNumEtape($numEtape)
    {
        $this->numEtape = $numEtape;

        return $this;
    }

    /**
     * Get numEtape
     *
     * @return string 
     */
    public function getNumEtape()
    {
        return $this->numEtape;
    }
}
