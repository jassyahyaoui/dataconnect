<?php

namespace WorkflowBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class QualificationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('existe', ChoiceType::class, array(
                        'choices'  => array(
                            true => 'Oui',
                            false => 'Non',
                     ),
                    'expanded' => true,
                    'multiple' => false
                    ))
                
                ->add('activity', EntityType::class, array(
                        'class' => 'WorkflowBundle:Activity',
                    )
                        )
                ->add('department', EntityType::class, array(
                        'class' => 'WorkflowBundle:Departement',
                    )
                        )
                ->add('commune')
                ->add('siret', TextType::class, ['mapped' => false])
                ->add('forme', EntityType::class, array(
                        'class' => 'WorkflowBundle:Forme',
                    )
                        )
                ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'workflowbundle_qualification';
    }


}
