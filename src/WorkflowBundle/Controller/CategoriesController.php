<?php

namespace WorkflowBundle\Controller;

use WorkflowBundle\Entity\Categories;
use WorkflowBundle\Form\QualificationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Category controller.
 *
 */
class CategoriesController extends Controller
{
    /**
     * Lists all category entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('WorkflowBundle:Categories')->findAll();

        return $this->render('WorkflowBundle:Categories:index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new category entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();        
        $category = new Categories();
        $form = $this->createForm('WorkflowBundle\Form\CategoriesType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $file = $category->getImage();
            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('image_directory'),
                $fileName
            );
            $category->setImage($fileName);
   
            $em->persist($category);
            $em->flush($category);

            return $this->redirectToRoute('categories_show', array('id' => $category->getId()));
        }

        return $this->render('WorkflowBundle:Categories:new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a category entity.
     *
     */
    public function showAction(Request $request, Categories $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $form = $this->createForm('WorkflowBundle\Form\CategoriesType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //test envoi formulzire
           dump($form->get('questions')->getData());die;
        }
        $formQualification = $this->createForm( new QualificationType());
        
        return $this->render('WorkflowBundle:Categories:show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
            'formQualification' => $formQualification->createView(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     */
    public function editAction(Request $request, Categories $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('WorkflowBundle\Form\CategoriesType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categories_edit', array('id' => $category->getId()));
        }

        return $this->render('WorkflowBundle:Categories:edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     */
    public function deleteAction(Request $request, Categories $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush($category);
        }

        return $this->redirectToRoute('categories_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Categories $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Categories $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categories_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
