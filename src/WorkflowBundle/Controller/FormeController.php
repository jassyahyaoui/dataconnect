<?php

namespace WorkflowBundle\Controller;

use WorkflowBundle\Entity\Forme;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Forme controller.
 *
 */
class FormeController extends Controller
{
    /**
     * Lists all forme entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $formes = $em->getRepository('WorkflowBundle:Forme')->findAll();

        return $this->render('WorkflowBundle:Forme:index.html.twig', array(
            'formes' => $formes,
        ));
    }

    /**
     * Creates a new forme entity.
     *
     */
    public function newAction(Request $request)
    {
        $forme = new Forme();
        $form = $this->createForm('WorkflowBundle\Form\FormeType', $forme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($forme);
            $em->flush($forme);

            return $this->redirectToRoute('forme_show', array('id' => $forme->getId()));
        }

        return $this->render('WorkflowBundle:Forme:new.html.twig', array(
            'forme' => $forme,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a forme entity.
     *
     */
    public function showAction(Forme $forme)
    {
        $deleteForm = $this->createDeleteForm($forme);

        return $this->render('WorkflowBundle:Forme:show.html.twig', array(
            'forme' => $forme,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing forme entity.
     *
     */
    public function editAction(Request $request, Forme $forme)
    {
        $deleteForm = $this->createDeleteForm($forme);
        $editForm = $this->createForm('WorkflowBundle\Form\FormeType', $forme);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('forme_edit', array('id' => $forme->getId()));
        }

        return $this->render('WorkflowBundle:Forme:edit.html.twig', array(
            'forme' => $forme,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a forme entity.
     *
     */
    public function deleteAction(Request $request, Forme $forme)
    {
        $form = $this->createDeleteForm($forme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($forme);
            $em->flush($forme);
        }

        return $this->redirectToRoute('forme_index');
    }

    /**
     * Creates a form to delete a forme entity.
     *
     * @param Forme $forme The forme entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Forme $forme)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('forme_delete', array('id' => $forme->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
