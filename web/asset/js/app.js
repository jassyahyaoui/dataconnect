$('document').ready(function(){    
    $c_company = $('.code_company');
        $.ajax({
              type:'GET',
              url: Routing.generate('data_connect_get_folder_list'),
              beforeSend: function(){
                  if($('.loading').length === 0){
                    $('form .code_folder').parent().append('<div class="loading"></div>');
                    }
              },
              success: function(data){
                  if(data.folders)
                  {
                      $('.code_folder')
                      .find('option')
                      .remove()
                      .end();
                      $.each(data.folders, function(key, value) {  
                          $('.code_folder')
                                   .append($('<option>', { value : value.id })
                                   .text(value.name));
                         });
                  }
                  $('.loading').remove();
              }
        });
      

//event
/*
if($c_company.length !== "")
{
    getCodeFolder($c_company);   
}

$c_company.bind("keyup change",function(){
   getCodeFolder($c_company);
});   */ 

});
